package test.thesis.felix.Classifiers.yolo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import thesis.felix.Classifiers.interfaces.IClassifierClassName;
import thesis.felix.Classifiers.yolo.Yolo3ClassNames;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


public class Yolo3ClassNamesTests {

    IClassifierClassName className;

    @BeforeEach
    /***
     * @brief Init the YoloClassNames object new for every test
     */
    void init() {
        this.className = new Yolo3ClassNames();
    }

    @Test
    @DisplayName("Testing if the 0 index gives a non zero response")
    void givingResponseForIndex() {
        assertNotEquals("", this.className.getClassNameForIndex(0));
    }

    @Test
    @DisplayName("Testing if car gives the correct index")
    void givingIndexForName() {
        assertEquals(2, this.className.getIndexForClassName("car"));
    }


}
