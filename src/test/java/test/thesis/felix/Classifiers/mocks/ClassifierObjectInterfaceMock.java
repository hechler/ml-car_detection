package test.thesis.felix.Classifiers.mocks;

import org.opencv.core.Size;
import org.opencv.dnn.Net;
import thesis.felix.Classifiers.interfaces.IClassifierClassName;
import thesis.felix.Classifiers.interfaces.IDnnClassifier;

public class ClassifierObjectInterfaceMock implements IDnnClassifier {
    @Override
    public void setModelWeightsFile(String modelWeightsFile) {

    }

    @Override
    public void setModelConfigurationFile(String modelConfigurationFile) {

    }

    @Override
    public String getModelWeightsFile() {
        return null;
    }

    @Override
    public String getModelConfigurationFile() {
        return null;
    }

    @Override
    public IDnnClassifier getInstance() {
        return null;
    }

    @Override
    public Net getNet() {
        return null;
    }

    @Override
    public Size getImageSize() {
        return null;
    }

    @Override
    public void setImageSize(Size size) {

    }

    @Override
    public IClassifierClassName getClassNameObject() {
        return null;
    }

    @Override
    public void setClassNameObject(IClassifierClassName object) {

    }
}
