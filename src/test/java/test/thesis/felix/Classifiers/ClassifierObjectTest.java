package test.thesis.felix.Classifiers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import test.thesis.felix.Classifiers.mocks.ClassifierObjectInterfaceMock;
import thesis.felix.Classifiers.ClassifierObject;
import thesis.felix.Classifiers.interfaces.IClassifierClassName;
import thesis.felix.Classifiers.yolo.Yolo3ClassNames;
import thesis.felix.Classifiers.yolo.Yolov3;

import java.lang.reflect.InvocationTargetException;

import static org.junit.jupiter.api.Assertions.*;

public class ClassifierObjectTest {

    ClassifierObject classifierObject;
    String testName = "testName";
    String testDescription = "testDescription";

    @BeforeEach
    /***
     * @brief Init the YoloClassNames object new for every test
     */
    void init() {
        this.classifierObject = new ClassifierObject(testName, testDescription, ClassifierObjectInterfaceMock.class);
    }

    @Test
    @DisplayName("Testing if name and Description are returned correctly")
    void returnFields() {
        assertEquals(this.testName, this.classifierObject.getName());
        assertEquals(this.testDescription, this.classifierObject.getDescription());
    }

    @Test
    @DisplayName("Testing if i'm returned the correct object ( getInstance() )")
    void testInterface() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        assertTrue(this.classifierObject.getInstance() instanceof ClassifierObjectInterfaceMock);
    }
}
