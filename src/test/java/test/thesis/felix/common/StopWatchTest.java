package test.thesis.felix.common;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import thesis.felix.Classifiers.yolo.Yolo3ClassNames;
import thesis.felix.Common.StopWatch;

import static org.junit.jupiter.api.Assertions.*;

public class StopWatchTest {

    StopWatch stopWatch;
    int waitTime = 10;

    @BeforeEach
    /***
     * @brief Init the YoloClassNames object new for every test
     */
    void init() {
        this.stopWatch = new StopWatch();
    }

    @Test
    @DisplayName("Testing if the stopwatch is correctly stopping one second")
    void TestingSimpleStopwatchFunction() throws InterruptedException {
        this.stopWatch.startWatch();

        Thread.sleep(this.waitTime);

        this.stopWatch.stopWatch();

        assertTrue(this.stopWatch.getTimeInMilliseconds() >= this.waitTime);
        assertTrue(this.stopWatch.getTimeInSeconds() >= (float) (this.waitTime / 1000));
    }

    @Test
    @DisplayName("Testing if double starting the stopwatch is not possible")
    void TestDoubleTimeTaking() throws InterruptedException {
        this.stopWatch.startWatch();

        Thread.sleep(this.waitTime);

        this.stopWatch.startWatch();

        this.stopWatch.stopWatch();

        assertTrue(this.stopWatch.getTimeInMilliseconds() >= this.waitTime);

    }
}
