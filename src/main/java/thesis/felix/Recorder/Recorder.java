package thesis.felix.Recorder;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.locks.ReadWriteLock;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.highgui.HighGui;
import org.opencv.videoio.VideoWriter;

import thesis.felix.Common.FrameObject;
import thesis.felix.Common.StopWatch;
import thesis.felix.Common.Config.ApplicationConfig;
import thesis.felix.ThreadControl.EndProgramm;

public class Recorder implements Runnable {

    private ReadWriteLock rwLock;
    ConcurrentLinkedDeque<FrameObject> queue;
    private boolean isRecording;
    private VideoWriter vw;
    private StopWatch sw;
    private int missing = 0;
    private boolean save = false;
    private String currentVideoFileName;
    private ApplicationConfig config = new ApplicationConfig();

    private int fps = this.config.getFPS();

    public Recorder(ReadWriteLock rwLock, ConcurrentLinkedDeque<FrameObject> queue) {
        this.rwLock = rwLock;
        this.queue = queue;
        this.isRecording = false;
        this.sw = new StopWatch();
    }

    private void createVideoWriter(Size size) {

        if (this.isRecording == false) {

            this.currentVideoFileName = String.format("%s%d.mp4", this.config.getOutputFolder(),
                    new File(this.config.getOutputFolder()).listFiles(File::isFile).length);

            this.vw = new VideoWriter();

            System.out.println(this.currentVideoFileName);

            this.vw.open(this.currentVideoFileName, VideoWriter.fourcc('M', 'J', 'P', 'G'), this.fps, size);

            System.out.println(this.vw.isOpened());

            this.isRecording = true;
        }

    }

    private void saveFrame(Mat frame) {
        this.createVideoWriter(new Size(frame.cols(), frame.rows()));
        this.vw.write(frame);

    }

    private void saveVideo() {
        if (this.isRecording) {

            this.vw.release();
            this.isRecording = false;
        }
        this.save = false;
    }

    private void deleteVideo() {
        if (this.isRecording) {
            File fileToDelete = new File(this.currentVideoFileName);
            fileToDelete.delete();
            this.isRecording = false;
        }
        this.save = false;
    }

    private void initRecording() {

        Timer recorder = new Timer();

        int missing = 0;

        this.createVideoWriter(
                new Size(this.queue.getFirst().getFrame().cols(), this.queue.getFirst().getFrame().rows()));

        recorder.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {

                if (!queue.isEmpty()) {
                    FrameObject frameObject = queue.removeFirst();

                    if (frameObject.getCarIsPresentInFrame()) {
                        resetCarIsMissing();
                        saveFrame(frameObject.getFrame());

                        if (frameObject.getIntersectsRoi()) {
                            save = true;
                        }
                    } else {
                        incrementCarIsMissing();

                        saveFrame(frameObject.getFrame());
                        if (getCarIsMissing() >= 30) {
                            recorder.cancel();
                            recorder.purge();

                            if (save) {
                                saveVideo();
                            } else {
                                deleteVideo();
                            }
                        }
                    }
                }
            }
        }, 0, (1000 / this.fps));

    }

    private void incrementCarIsMissing() {
        this.missing++;
    }

    private void resetCarIsMissing() {
        this.missing = 0;
    }

    private int getCarIsMissing() {
        return this.missing;
    }

    @Override
    public void run() {
        int framesCarIsMissing = 0;

        while (true) {

            while (!this.isRecording && queue.stream().filter((obj) -> obj.getEvaluated()).count() > 0) {
                FrameObject frameObject = this.queue.getFirst();

                if (frameObject.getCarIsPresentInFrame()) {
                    this.initRecording();

                } else { // delete Frame
                    if (!this.queue.isEmpty()) {
                        this.queue.removeFirst();
                    }
                }

            }

            if (this.checkIfTerminate()) {
                try {
                    while (!this.queue.isEmpty()) {
                        FrameObject frameObject = this.queue.removeFirst();

                        if (frameObject.getCarIsPresentInFrame()) {
                            // i'm not recording but a car is present in the Frame

                            this.saveFrame(frameObject.getFrame());
                            framesCarIsMissing = 0;

                        } else {
                            framesCarIsMissing++;

                            if (framesCarIsMissing >= 30) {
                                this.saveVideo();

                            }
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                this.saveVideo();
                return;
            }
        }

    }

    private boolean checkIfTerminate() {
        this.rwLock.readLock().lock();

        boolean re = EndProgramm.endProgramm;

        this.rwLock.readLock().unlock();

        return re;
    }

    // public void placeHolder() {
    // if (response.getCarIsPresent()) {

    // recordingStarted = true;
    // framesCarIsMissing = 0;

    // HighGui.imshow("eval", response.getFrame());
    // HighGui.waitKey(1);
    // } else {
    // if (recordingStarted) {
    // framesCarIsMissing++;

    // if (framesCarIsMissing > 30) {

    // VideoWriter vw = new VideoWriter(
    // String.format("/Volumes/Data/output/%d.mp4",
    // new File("/Volumes/Data/output/").listFiles(File::isFile).length + 1),
    // VideoWriter.fourcc('X', 'V', 'I', 'D'), 60, new Size(frame.cols(),
    // frame.rows()));
    // for (int i = 0; i < size; i++) {

    // vw.write(this.queue.removeFirst().getFrame());
    // }

    // vw.release();
    // recordingStarted = false;
    // }
    // }
    // }
    // }

}
