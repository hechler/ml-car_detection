package thesis.felix.Recorder;

public class RecorderThreadController {
    public static boolean isRecording = false;
    public static boolean startRecording = false;
    public static boolean stopRecording = false;
    public static boolean saveRecording = false;
    public static int numberOfFrames = 0;
}