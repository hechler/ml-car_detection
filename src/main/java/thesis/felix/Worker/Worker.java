package thesis.felix.Worker;

import org.opencv.core.*;
import org.opencv.dnn.Dnn;
import org.opencv.dnn.Net;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;
import org.opencv.videoio.VideoWriter;

import thesis.felix.Classifiers.ClassifierFactory;
import thesis.felix.Classifiers.ClassifierNames;
import thesis.felix.Classifiers.ClassifierObject;
import thesis.felix.Classifiers.interfaces.IClassifierClassName;
import thesis.felix.Classifiers.interfaces.IClassifierFactory;
import thesis.felix.Classifiers.interfaces.IDnnClassifier;
import thesis.felix.Comparison.CompareResult;
import thesis.felix.Recorder.Recorder;
import thesis.felix.ThreadControl.EndProgramm;
import thesis.felix.Main;
import thesis.felix.Tracker.ObjectTracker;
import thesis.felix.Common.Camera;
import thesis.felix.Common.FrameObject;
import thesis.felix.Common.ScannerClean;
import thesis.felix.Common.StopWatch;
import thesis.felix.Common.Config.ApplicationConfig;
import thesis.felix.interfaces.IStopWatch;

import java.awt.geom.Point2D;
import java.awt.image.CropImageFilter;
import java.io.File;
import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Worker implements Runnable {

    private Vector<Integer> indexOfNetsToCompare = new Vector<>();
    private Map<String, IDnnClassifier> nets = new HashMap<>();
    private ScannerClean scanner = ScannerClean.getInstance();
    private String inputPath = "";
    private int imageLimit = 0;
    private ObjectTracker objectTracker;
    ConcurrentLinkedDeque<FrameObject> queue;
    ReadWriteLock rwLockProgrammEnd;
    private ApplicationConfig config = new ApplicationConfig();

    private ArrayList<Point> points = new ArrayList<>();

    public Worker(boolean non, ConcurrentLinkedDeque<FrameObject> queue, ReadWriteLock rwLock) {

        this.rwLockProgrammEnd = rwLock;
        this.queue = queue;

        Camera camera = new Camera();

        this.objectTracker = new ObjectTracker(300, camera);

        IClassifierFactory factory = ClassifierFactory.getInstance();
        int netIndex = this.config.getNN();

        try {
            Field[] fields = ClassifierNames.class.getFields();

            Field field = ClassifierNames.class.getField(fields[netIndex].getName());

            this.nets.put(fields[netIndex].getName(),
                    factory.getNetwork((ClassifierObject) field.get(ClassifierNames.class)));

            System.out.println(fields[netIndex].getName());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Worker() {// todo refactor: split up

        Field[] fields = ClassifierNames.class.getFields();

        System.out.println(String.format("How many nets you want to compare? (1 - %d)", fields.length));

        int amountToCompare = this.scanner.nextInt();

        for (int i = 0; i < amountToCompare; i++) {
            System.out.println("Enter Number for NN");
            for (int j = 0; j < fields.length; j++) {

                if (!indexOfNetsToCompare.contains(j)) {
                    System.out.println(String.format("%d : %s", j, fields[j].getName()));

                }
            }
            this.indexOfNetsToCompare.add(this.scanner.nextInt());

        }

        IClassifierFactory factory = ClassifierFactory.getInstance();

        this.indexOfNetsToCompare.forEach((value) -> {
            try {

                Field field = ClassifierNames.class.getField(fields[value].getName());

                this.nets.put(fields[value].getName(),
                        factory.getNetwork((ClassifierObject) field.get(ClassifierNames.class)));

            } catch (Exception e) {
                e.printStackTrace();
            }

        });

    }

    void setPathOfImage(String path) {
        this.inputPath = path;
    }

    public void setPathOfIamgeFolderByInput() {
        System.out.println("Enter path");

        this.setPathOfImage(this.scanner.nextLine());
    }

    void setInputLimit(int limit) {
        this.imageLimit = Math.min(limit,
                Objects.requireNonNull(new File(this.inputPath).listFiles(File::isFile)).length);
    }

    public void setInputLimitByInput() {

        System.out.println(String.format("Enter limit(Hard Limit: %d)",
                Objects.requireNonNull(new File(this.inputPath).listFiles(File::isFile)).length));

        this.setInputLimit(this.scanner.nextInt()); // Read user input
    }

    public EvaluatedFrameResponse eval(Mat frame) {
        Camera camera = new Camera();

        boolean intersectsRoi = false;

        boolean carIsMoving = false;

        for (Map.Entry<String, IDnnClassifier> entry : this.nets.entrySet()) {

            String classifierName = entry.getKey();
            IDnnClassifier classifier = entry.getValue();

            IClassifierClassName classNames = classifier.getClassNameObject();

            Net net = classifier.getNet();

            // Imgproc.resize(frame, frame, classifier.getImageSize());

            List<Mat> result = new ArrayList<>();
            List<String> outBlobNames = Main.getOutputNames(net);

            Mat blob = Dnn.blobFromImage(frame, 0.00392, classifier.getImageSize(), new Scalar(0), true, false);

            net.setInput(blob);

            // Feed forward the model to get output //
            outBlobNames.forEach((name) -> {
                // result.add(net.forward(name));

            });

            result.add(net.forward(outBlobNames.get(0)));

            // outBlobNames.forEach(System.out::println);
            // result.forEach(System.out::println);

            float confThreshold = 0.5f;
            List<Integer> clsIds = new ArrayList<>();
            List<Float> confs = new ArrayList<>();
            List<Rect2d> rects = new ArrayList<>();
            List<Rect2d> rectList = new ArrayList<>();

            List<Point3> points = new LinkedList<>();

            for (Mat level : result) {

                for (int j = 0; j < level.rows(); ++j) {

                    Mat row = level.row(j);
                    Mat scores = row.colRange(5, level.cols());
                    Core.MinMaxLocResult mm = Core.minMaxLoc(scores);
                    float confidence = (float) mm.maxVal;
                    Point classIdPoint = mm.maxLoc;
                    if (confidence > confThreshold) {

                        int centerX = (int) (row.get(0, 0)[0] * frame.cols());

                        int centerY = (int) (row.get(0, 1)[0] * frame.rows());

                        int width = Math.min(frame.cols() - 1, (int) (row.get(0, 2)[0] * frame.cols()));
                        int height = Math.min(frame.rows() - 1, (int) (row.get(0, 3)[0] * frame.rows()));

                        int left = Math.max(0, centerX - width / 2);
                        int top = Math.max(0, centerY - height / 2);

                        clsIds.add((int) classIdPoint.x);

                        confs.add((float) confidence);
                        Rect2d tmp = new Rect2d(left, top, width, height);
                        rects.add(tmp);
                    }
                }

                if (confs.isEmpty()) {
                    objectTracker.markFrameWithoutObject();

                    HighGui.imshow("eval", frame);
                    HighGui.waitKey(1);

                    return new EvaluatedFrameResponse(null, false, false);

                }

                float nmsThresh = 0.6f;

                MatOfFloat confidences = new MatOfFloat(Converters.vector_float_to_Mat(confs));
                Rect2d[] boxesArray = rects.toArray(new Rect2d[0]);
                MatOfRect2d boxes = new MatOfRect2d(boxesArray);
                MatOfInt indices = new MatOfInt();
                Dnn.NMSBoxes(boxes, confidences, confThreshold, nmsThresh, indices);

                int[] ind = indices.toArray();

                for (int i = 0; i < ind.length; ++i) {
                    int idx = ind[i];

                    Rect2d box = boxesArray[idx];

                    rectList.add(box);

                    Point centerOfRect = new Point(box.x + (box.width * 0.5), box.y + (box.height * 0.5));

                    Imgproc.rectangle(frame, box.tl(), box.br(), new Scalar(0, 255, 0), 5);
                    Imgproc.circle(frame, centerOfRect, 5, new Scalar(0, 0, 255), -1);
                    Imgproc.putText(frame,
                            String.format("%s , Conf. : %f", classNames.getClassNameForIndex(clsIds.get(i)),
                                    confs.get(i)),
                            new Point(rects.get(i).x, rects.get(i).y), 16, 2, new Scalar(0, 0, 255), 2);

                    // System.out.printf("x%f : y%f : z%f\n", centerOfRect.x, centerOfRect.y,
                    // camera.getDistance(box.height) / 1000);

                    points.add(new Point3(centerOfRect.x, centerOfRect.y, camera.getDistance((int) box.height) / 1000));

                }
            }

            this.objectTracker.getIDForObject(frame, points);

            // Imgproc.resize(frame, frame, new Size(1500, 720));
            HighGui.imshow("eval", frame);
            HighGui.waitKey(1);

            Rect rect = this.config.getRoi();

            intersectsRoi = this.objectTracker.isAnyCarStandingInRoi(rect);

            // intersectsRoi = points.stream().anyMatch(obj -> rect.contains(new
            // Point(obj.x, obj.y)));

            carIsMoving = this.objectTracker.isAnyCarMoving();

        }

        return new EvaluatedFrameResponse(frame, carIsMoving, intersectsRoi);

    }

    @Override
    public void run() {
        StopWatch sw = new StopWatch();
        EvaluatedFrameResponse response;
        while (true) {
            try {

                FrameObject frameObject = this.queue.getLast();

                if (frameObject.getEvaluated()) {

                    continue;
                }

                Mat frame = frameObject.getFrame();

                response = this.eval(frame.clone());

                if (response.getCarIsPresent()) {
                    frameObject.setCarIsPresentInFrame();

                    if (response.getIntersectsRoi()) {
                        frameObject.setInterserctsRoi();
                    }
                }
                frameObject.setEvaluated();
            } catch (Exception e) {

            }

            if (this.checkIfTerminate()) {
                HighGui.destroyAllWindows();
                System.out.println("End Worker");
                return;
            }
        }

    }

    private boolean checkIfTerminate() {
        this.rwLockProgrammEnd.readLock().lock();

        boolean re = EndProgramm.endProgramm;

        this.rwLockProgrammEnd.readLock().unlock();

        return re;
    }

}

class EvaluatedFrameResponse {
    private Mat frame;
    private boolean carIsPresent = false;
    private boolean intersectsRoi = false;

    EvaluatedFrameResponse(Mat frame, boolean carIsPresent, boolean intersectsRoi) {
        this.frame = frame;
        this.carIsPresent = carIsPresent;
        this.intersectsRoi = intersectsRoi;

    }

    public Mat getFrame() {
        return this.frame;
    }

    public boolean getCarIsPresent() {
        return this.carIsPresent;
    }

    public boolean getIntersectsRoi() {
        return this.intersectsRoi;
    }

}
