package thesis.felix.Classifiers;

import org.opencv.dnn.Net;
import thesis.felix.Classifiers.interfaces.IClassifierFactory;
import thesis.felix.Classifiers.interfaces.IDnnClassifier;
import thesis.felix.Classifiers.yolo.Yolov3;
import thesis.felix.Classifiers.yolo.Yolov3Tiny;

import java.lang.reflect.InvocationTargetException;


public class ClassifierFactory implements IClassifierFactory {

    private static ClassifierFactory instance;

    /***
     * @brief Singleton ClassifierFactory
     */
    private ClassifierFactory() {

    }

    public static IClassifierFactory getInstance() {
        if (instance == null) {
            instance = new ClassifierFactory();
        }
        return instance;
    }

    /***
     * @brief Will return the requested Network
     * @param object
     * @return Net
     */
    public IDnnClassifier getNetwork(ClassifierObject object) throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {
            return object.getInstance();
    }


}
