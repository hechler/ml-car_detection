package thesis.felix.Classifiers.yolo;

import org.opencv.core.Size;
import org.opencv.dnn.Dnn;
import thesis.felix.Classifiers.DarknetClassifier;

public class Yolov3Tiny extends DarknetClassifier {

    public Yolov3Tiny() {
        this.setModelConfigurationFile("extras/yolov3-tiny.cfg");
        this.setModelWeightsFile("extras/yolov3-tiny.weights");

        this.setImageSize(new Size(640, 640));

        this.setNet(Dnn.readNetFromDarknet(this.getModelConfigurationFile(), this.getModelWeightsFile()));


        this.setClassNameObject(new Yolo3ClassNames());
    }
}
