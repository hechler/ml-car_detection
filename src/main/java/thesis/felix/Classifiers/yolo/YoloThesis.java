package thesis.felix.Classifiers.yolo;

import org.opencv.core.Size;
import org.opencv.dnn.Dnn;
import org.opencv.dnn.Net;
import thesis.felix.Classifiers.DarknetClassifier;

public class YoloThesis extends DarknetClassifier {

    public YoloThesis(){
        this.setModelConfigurationFile("extras/my.cfg");
        this.setModelWeightsFile("extras/my_last.weights");

        this.setImageSize(new Size(640, 640));

        Net net = Dnn.readNetFromDarknet(this.getModelConfigurationFile(), this.getModelWeightsFile());

        this.setNet(net);

        this.setClassNameObject(new Yolo3ThesisClassNames());
    }
}
