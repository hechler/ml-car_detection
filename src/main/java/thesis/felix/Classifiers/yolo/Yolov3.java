package thesis.felix.Classifiers.yolo;

import org.opencv.core.Size;
import org.opencv.dnn.Dnn;
import thesis.felix.Classifiers.DarknetClassifier;
import thesis.felix.Classifiers.interfaces.IClassifierClassName;

public class Yolov3 extends DarknetClassifier {

    public Yolov3() {
        this.setModelConfigurationFile("extras/yolov3.cfg");
        this.setModelWeightsFile("extras/yolov3.weights");

        this.setImageSize(new Size(640, 640));

        this.setNet(Dnn.readNetFromDarknet(this.getModelConfigurationFile(), this.getModelWeightsFile()));

        this.setClassNameObject(new Yolo3ClassNames());
    }
}
