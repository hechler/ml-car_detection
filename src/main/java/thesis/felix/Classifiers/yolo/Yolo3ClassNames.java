package thesis.felix.Classifiers.yolo;

import thesis.felix.Classifiers.interfaces.IClassifierClassName;

import java.util.Arrays;
import java.util.Vector;

public class Yolo3ClassNames implements IClassifierClassName {

    /***
     * @brief Will represent the Classname at a given index
     */
    protected Vector<String> classNames;

    /***
     * @brief Constructor with all class names
     */
    public Yolo3ClassNames() {
        this.classNames = new Vector<>();
        String[] names = {
                "person",
                "bicycle",
                "car",
                "motorcycle",
                "airplane",
                "bus",
                "train",
                "truck",
                "boat",
                "traffic light",
                "fire hydrant",
                "stop sign",
                "parking meter",
                "bench",
                "bird",
                "cat",
                "dog",
                "horse",
                "sheep",
                "cow",
                "elephant",
                "bear",
                "zebra",
                "giraffe",
                "backpack",
                "umbrella",
                "handbag",
                "tie",
                "suitcase",
                "frisbee",
                "skis",
                "snowboard",
                "sports ball",
                "kite",
                "baseball bat",
                "baseball glove",
                "skateboard",
                "surfboard",
                "tennis racket",
                "bottle",
                "wine glass",
                "cup",
                "fork",
                "knife",
                "spoon",
                "bowl",
                "banana",
                "apple",
                "sandwich",
                "orange",
                "broccoli",
                "carrot",
                "hot dog",
                "pizza",
                "donut",
                "cake",
                "chair",
                "couch",
                "potted plant",
                "bed",
                "dining table",
                "toilet",
                "tv",
                "laptop",
                "mouse",
                "remote",
                "keyboard",
                "cell phone",
                "microwave",
                "oven",
                "toaster",
                "sink",
                "refrigerator",
                "book",
                "clock",
                "vase",
                "scissors",
                "teddy bear",
                "hair drier",
                "toothbrush"
        };

        this.classNames.addAll(Arrays.asList(names));
    }

    /***
     * @brief Will return the Classname by index
     * @param index
     * @return
     */
    @Override
    public String getClassNameForIndex(int index) {
        return this.classNames.get(index);
    }

    /***
     * @brief Will return the Index by Classname
     * @param name
     * @return
     */
    @Override
    public int getIndexForClassName(String name) {
        return this.classNames.indexOf(name.toLowerCase());
    }


}
