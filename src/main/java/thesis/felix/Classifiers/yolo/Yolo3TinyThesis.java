package thesis.felix.Classifiers.yolo;

import org.opencv.core.Size;
import org.opencv.dnn.Dnn;
import org.opencv.dnn.Net;
import thesis.felix.Classifiers.DarknetClassifier;

public class Yolo3TinyThesis extends DarknetClassifier {

    public Yolo3TinyThesis() {
        this.setModelConfigurationFile("extras/my-tiny.cfg");
        this.setModelWeightsFile("extras/my-tiny_last.weights");

        this.setImageSize(new Size(480, 480));

        Net net = Dnn.readNetFromDarknet(this.getModelConfigurationFile(), this.getModelWeightsFile());

        this.setNet(net);

        this.setClassNameObject(new Yolo3ThesisClassNames());
    }
}
