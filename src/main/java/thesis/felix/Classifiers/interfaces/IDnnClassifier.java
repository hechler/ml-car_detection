package thesis.felix.Classifiers.interfaces;

import org.opencv.core.Size;
import org.opencv.dnn.Net;

/***
 * @brief Generic interface classifiers need to support
 */
public interface IDnnClassifier {

    void setModelWeightsFile(String modelWeightsFile);

    void setModelConfigurationFile(String modelConfigurationFile);

    String getModelWeightsFile();

    String getModelConfigurationFile();

    IDnnClassifier getInstance();

    Net getNet();

    Size getImageSize();

    void setImageSize(Size size);

    IClassifierClassName getClassNameObject();

    void setClassNameObject(IClassifierClassName object);
}
