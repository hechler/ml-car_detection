package thesis.felix.Classifiers.interfaces;

public interface IClassifierClassName {


    /***
     * @brief Will return the Classname by index
     * @param index
     * @return
     */
    public String getClassNameForIndex(int index);


    /***
     * @brief Will return the Index by Classname
     * @param name
     * @return
     */
    public int getIndexForClassName(String name);


}
