package thesis.felix.Classifiers.interfaces;

import org.opencv.dnn.Net;
import thesis.felix.Classifiers.ClassifierObject;

import java.lang.reflect.InvocationTargetException;

public interface IClassifierFactory {

    public IDnnClassifier getNetwork(ClassifierObject object) throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException;

}
