package thesis.felix.Classifiers;

import org.opencv.core.Size;
import org.opencv.dnn.Net;
import thesis.felix.Classifiers.interfaces.IClassifierClassName;
import thesis.felix.Classifiers.interfaces.IDnnClassifier;

public class DarknetClassifier implements IDnnClassifier {


    private String modelWeightsFile;
    private String modelConfigurationFile;
    private Net network;
    private Size imageSize;
    private IClassifierClassName classNameObject;

    /***
     * @brief Setter for the modelWeightsFile
     * @param modelWeightsFile String with path to file
     */
    @Override
    public void setModelWeightsFile(String modelWeightsFile) {
        this.modelWeightsFile = modelWeightsFile;
    }

    /***
     * @brief Setter for the modelConfigurationFile
     * @param modelConfigurationFile String with path to file
     */
    @Override
    public void setModelConfigurationFile(String modelConfigurationFile) {
        this.modelConfigurationFile = modelConfigurationFile;
    }


    /***
     * @brief Getter for the modelWeightsFile
     * @return String with path to file
     */
    @Override
    public String getModelWeightsFile() {
        return this.modelWeightsFile;
    }

    /***
     * @brief Getter for the modelConfigurationFile
     * @return String with path to file
     */
    @Override
    public String getModelConfigurationFile() {
        return this.modelConfigurationFile;
    }

    /***
     * @brief Will return the current object
     * @return
     */
    @Override
    public IDnnClassifier getInstance() {
        return this;
    }

    /***
     * @brief Will return the network of my type
     * @return
     */
    @Override
    public Net getNet() {
        return this.network;
    }

    /***
     * @brief Setter for the network
     * @param network
     */
    protected void setNet(Net network) {
        this.network = network;
    }

    /***
     * @brief Getter for the image size
     * @return
     */
    @Override
    public Size getImageSize() {
        return this.imageSize;
    }

    /***
     * @brief Setter for the image size
     * @param size
     */
    @Override
    public void setImageSize(Size size) {
        this.imageSize = size;
    }

    @Override
    public IClassifierClassName getClassNameObject() {
        return this.classNameObject;
    }

    public void setClassNameObject(IClassifierClassName object) {
        this.classNameObject = object;
    }
}
