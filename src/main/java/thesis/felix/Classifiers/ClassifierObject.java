package thesis.felix.Classifiers;

import thesis.felix.Classifiers.interfaces.IDnnClassifier;

import java.lang.reflect.InvocationTargetException;

/***
 * @brief The object is using Reflections to represent a Classifier as named in ClassifierNames class
 */
public class ClassifierObject {
    private final String name;
    private final Class<? extends IDnnClassifier> object;
    private final String description;

    public ClassifierObject(String name, String description, Class<? extends IDnnClassifier> object) {
        this.name = name;
        this.description = description;
        this.object = object;
    }

    /***
     * @brief Returing the name onf the classifier
     * @return
     */
    public String getName() {
        return this.name;
    }

    /***
     * Returning the description of the classifier
     * @return
     */
    public String getDescription() {
        return this.description;
    }

    /***
     * @brief Returning the object representing the choosen classifier
     * @return
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws InstantiationException
     */
    public IDnnClassifier getInstance() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        return this.object.getDeclaredConstructor().newInstance();
    }

}
