package thesis.felix.Classifiers;

import thesis.felix.Classifiers.yolo.YoloThesis;
import thesis.felix.Classifiers.yolo.Yolov3;
import thesis.felix.Classifiers.yolo.Yolov3Tiny;
import thesis.felix.Classifiers.yolo.Yolo3TinyThesis;

import java.lang.reflect.Constructor;

/***
 * @brief Usable networks. Are used via reflections so the user can choose one network
 */
public class ClassifierNames {
    public static final ClassifierObject YoloV3Tiny = new ClassifierObject("yolov3-tiny", "Fast but unreliable", Yolov3Tiny.class);
    public static final ClassifierObject YoloV3 = new ClassifierObject("yolov3", "Fast but reliable", Yolov3.class);
    public static final ClassifierObject YoloV3Thesis = new ClassifierObject("yolov3Thesis", "my own classifier", YoloThesis.class);
    public static final ClassifierObject YoloV3TinyThesis = new ClassifierObject("yolov3-tinyThesis", "slow and unreliable", Yolo3TinyThesis.class);
}

