package thesis.felix.interfaces;

public interface IStopWatch {

    void reset();

    void startWatch();

    void stopWatch();

    long getTimeInMilliseconds();

    float getTimeInSeconds();


}
