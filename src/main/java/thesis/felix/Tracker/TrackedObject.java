package thesis.felix.Tracker;

import java.util.Queue;

import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.opencv.core.Point3;

public class TrackedObject {

    public Point3 point;
    private final long id;
    private long lastUpdate;
    private Queue<Double> distanceQueue;
    private boolean moving;

    public TrackedObject(long id, Point3 point) {
        this.point = point;
        this.id = id;
        this.lastUpdate = 0;
        this.distanceQueue = new CircularFifoQueue<>(20);
        this.moving = false;
    }

    public void incrementLastUpdate() {
        this.lastUpdate--;
    }

    public long getLastUpdate() {
        return this.lastUpdate;
    }

    /***
     * @brief: Will return the dinstance from a given point to the object in 3d
     *         space
     * @param point
     * @return
     */
    public double getDistanceToPoint(Point3 point) {
        return Math.sqrt(Math.pow(this.point.x - point.x, 2) + Math.pow(this.point.y - point.y, 2)
                + Math.pow(this.point.z - point.z, 2));
    }

    /***
     * will return the movement status true = moving false = standing
     * 
     * @return
     */
    public boolean getMovingStatus() {
        return this.moving;
    }

    /***
     * Will update the current object, wich means it was found in the newest frame
     * 
     * @param newPoint
     * @return
     */
    public String getIdAndUpdate(Point3 newPoint) {
        this.lastUpdate = 0;
        this.distanceQueue.add(this.getDistanceToPoint(newPoint));

        double distanceValuesAdded = this.distanceQueue.stream().reduce(0.0, Double::sum);

        System.out.println(distanceValuesAdded / this.distanceQueue.size());

        if (distanceValuesAdded / this.distanceQueue.size() > 3) {
            this.moving = true;
        } else {
            this.moving = false;
        }

        this.point = newPoint;
        return String.valueOf(this.id);
    }

}
