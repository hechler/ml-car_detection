package thesis.felix.Tracker;

import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.lang3.tuple.MutablePair;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Point3;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import thesis.felix.Common.Camera;
import thesis.felix.Tracker.Interfaces.ITracker;

public class ObjectTracker implements ITracker {

    private List<TrackedObject> trackedObjectList;
    private double maxDistance;
    Camera camera;
    private long id;

    public ObjectTracker(double maxDistance, Camera camera) {
        this.trackedObjectList = new LinkedList<>();
        this.camera = camera;
        this.maxDistance = maxDistance;
        this.id = 1;
    }

    /**
     * Will add the corrosponding id on a Frame for given rectangles Will search the
     * shortest pairs of known points from the last frame and the new points from
     * the current frame
     * 
     * @param frame
     * @param points
     */
    public void getIDForObject(Mat frame, List<Point3> points) {

        List<TemporaryStructure> helperListPoints = new LinkedList<>();

        for (Point3 rect : points) {
            helperListPoints.add(new TemporaryStructure(rect));
        }

        /// clone the original list
        List<TrackedObject> helperTrackedObjects = new LinkedList<TrackedObject>(this.trackedObjectList);

        this.trackedObjectList.removeIf((trackedObject) -> trackedObject.getLastUpdate() < -15);

        int iterations = Math.min(this.trackedObjectList.size(), helperListPoints.size());

        for (int i = 0; i < iterations; i++) {
            this.trackedObjectList.get(i).incrementLastUpdate();

            double shortesDinstance = Double.MAX_VALUE;
            MutablePair<TrackedObject, TemporaryStructure> shortestPair = new MutablePair<>();

            for (TrackedObject trackedObject : helperTrackedObjects) {
                for (TemporaryStructure temporaryStructure : helperListPoints) {
                    double distance = trackedObject.getDistanceToPoint(temporaryStructure.point);

                    if (distance < shortesDinstance) {
                        shortesDinstance = distance;
                        shortestPair.setLeft(trackedObject);
                        shortestPair.setRight(temporaryStructure);
                    }

                }
            }

            // use the shortest pair
            helperTrackedObjects.remove(shortestPair.getLeft());
            helperListPoints.remove(shortestPair.getRight());

            String id = shortestPair.getLeft().getIdAndUpdate(shortestPair.getRight().point);

            Imgproc.putText(frame, id, new Point(shortestPair.getRight().point.x - 50, shortestPair.getRight().point.y),
                    16, 2, new Scalar(0, 0, 255), 2);
        }

        /// add a new id since the point could not be matched
        helperListPoints.forEach((helper) -> {
            if (helper.foundId == false) {
                TrackedObject tmp = new TrackedObject(this.getId(), helper.point);
                this.trackedObjectList.add(tmp);

                Imgproc.putText(frame, tmp.getIdAndUpdate(helper.point), new Point(helper.point.x - 50, helper.point.y),
                        16, 2, new Scalar(0, 0, 255), 2);
            }
        });

    }

    /***
     * @brief Method to call if there was a Frame update but no object was detected
     */
    public void markFrameWithoutObject() {
        this.trackedObjectList.forEach(action -> {
            action.incrementLastUpdate();
        });
    }

    /***
     * @brief Will determine if any car is actually moving in the current frame or
     *        all objects are standing still
     * @return True if mooving, False otherwise
     */
    public boolean isAnyCarMoving() {
        return this.trackedObjectList.stream().anyMatch((obj) -> {
            return obj.getMovingStatus();
        });
    }

    /***
     * Will check if any object is standing almost still in the current ROI
     * 
     * @param rectToProve
     * @return
     */
    public boolean isAnyCarStandingInRoi(Rect rectToProve) {
        return this.trackedObjectList.stream().anyMatch((obj) -> {
            return rectToProve.contains(new Point(obj.point.x, obj.point.y)) && !obj.getMovingStatus();
        });
    }

    private long getId() {
        return this.id++;
    }

    class TemporaryStructure {
        Point3 point;
        boolean foundId;
        TreeMap<Double, TrackedObject> distancesToIdsInOrder = new TreeMap<>();

        TemporaryStructure(Point3 rect) {
            this.point = rect;
            this.foundId = false;
        }

        public void markAsFound() {
            this.foundId = true;
        }
    }
}
