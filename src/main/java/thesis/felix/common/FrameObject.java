package thesis.felix.Common;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.opencv.core.Mat;

public class FrameObject {

    private Mat frame;
    private boolean evaluated;
    private boolean carIsPresentInFrame;
    private boolean intersectsRoi;

    public FrameObject(Mat frame) {
        this.frame = frame;
    }

    /***
     * Mark that the frame was evaluated by the neural network
     */
    public void setEvaluated() {
        this.evaluated = true;
    }

    public void setInterserctsRoi() {
        this.intersectsRoi = true;
    }

    public boolean getIntersectsRoi() {
        return this.intersectsRoi;
    }

    /***
     * Mark that the frame has cars present in it
     */
    public void setCarIsPresentInFrame() {
        this.carIsPresentInFrame = true;
    }

    public boolean getEvaluated() {
        return this.evaluated;
    }

    public boolean getCarIsPresentInFrame() {
        return this.carIsPresentInFrame;
    }

    public Mat getFrame() {
        return this.frame;
    }
}