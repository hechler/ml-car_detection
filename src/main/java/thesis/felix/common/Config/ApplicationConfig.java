package thesis.felix.Common.Config;

import java.lang.reflect.Field;

import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.videoio.VideoCapture;

import thesis.felix.ApplicationConfig.TakeRoi;
import thesis.felix.Classifiers.ClassifierNames;
import thesis.felix.Common.ScannerClean;
import thesis.felix.Common.VideoCaptureGlobal;

public class ApplicationConfig extends ConfigBase {
    public final String ROI = "roi";
    private final String VIDEOCAPTURE = "videoCapture";
    private final String OUTPUTFOLDER = "outputFolder";
    private final String NETWORK = "networkId";
    private final String FPS = "fps";
    private ScannerClean scanner = ScannerClean.getInstance();

    public ApplicationConfig() {
        super("config.properties");

    }

    /***
     * @brief: interactive wizard that will lead through the configuration process
     */
    public void setApplicationConfig() {

        this.setOutputFolderFromUser();

        this.getFpsOfVideoFromUser();

        this.setVideoCaptureConfig();

        this.setRoi();

        this.setNNByInput();

    }

    private void setNNByInput() {
        System.out.println("Enter Number for NN");

        Field[] networks = ClassifierNames.class.getFields();

        for (int i = 0; i < networks.length; i++) {
            System.out.println(String.format("%d : %s", i, networks[i].getName()));
        }

        this.setNN(this.scanner.nextLine());

    }

    private void setNN(String idOfNetwork) {
        this.setConfigValue(this.NETWORK, idOfNetwork);
    }

    public int getNN() {
        return Integer.parseInt(getConfigValue(this.NETWORK));
    }

    private void getFpsOfVideoFromUser() {
        System.out.println("Whats the fps of the video feed");

        this.setFPS(this.scanner.nextLine());
    }

    /***
     * 
     */
    private void setOutputFolderFromUser() {
        System.out.println("Where do you want you video clips to be stored?");

        this.setOutputFolder(this.scanner.nextLine());

    }

    private void setRoi() {
        TakeRoi roi = new TakeRoi();

        Mat tmpFrame = new Mat();

        VideoCaptureGlobal.getInstance().read(tmpFrame);

        System.out.println("A window will open. Choose your ROI and press enter");

        this.setRoi(roi.placeHolder(tmpFrame));
    }

    private void setVideoCaptureConfig() {
        System.out.printf("Do you want to use a video file or a connected camera? \n 0: File \t 1: Camera\n");

        int decision = this.scanner.nextInt();

        if (decision == 0) {
            /// video file config
            System.out.println("Please set the path for the video file:");

            this.setVideoCapture(this.scanner.nextLine());
        } else if (decision == 1) {
            /// webcam config
            int amountOfWebcams = getAmountOfWebcams();
            if (amountOfWebcams <= 0) {
                System.out.println(
                        "I can't find any webcam. Either there is none installed, or i do not have permission to access the webcam.");
            } else {
                System.out.println("Wich camera connected to the PC to you want to use ?");
                for (int i = 0; i < amountOfWebcams; i++) {
                    System.out.printf("%d \n", i);
                }
                this.setVideoCapture(this.scanner.nextLine());
            }
        } else {
            System.out.println("Sorry your choise was not available: exiting");
            System.exit(0);
        }

    }

    private int getAmountOfWebcams() {
        int amountOfDevices = 0;
        VideoCapture vc = new VideoCapture();
        vc.setExceptionMode(true);
        while (true) {

            try {
                vc.open(amountOfDevices);
                vc.release();
            } catch (Exception e) {
                vc.release();
                return amountOfDevices;
            }
            amountOfDevices++;
        }
    }

    public void setRoi(Rect rect) {
        this.setConfigValue(this.ROI, String.format("%s,%s,%s,%s", (int) rect.tl().x, (int) rect.tl().y,
                (int) rect.br().x, (int) rect.br().y));
    }

    public Rect getRoi() {
        String[] configValue = this.getConfigValue(this.ROI).split(",");

        return new Rect(Integer.parseInt(configValue[0]), Integer.parseInt(configValue[1]),
                Integer.parseInt(configValue[2]), Integer.parseInt(configValue[3]));
    }

    public void setVideoCapture(String videoCaptureValue) {
        this.setConfigValue(this.VIDEOCAPTURE, videoCaptureValue);
    }

    public String getVideoCapture() {
        return this.getConfigValue(this.VIDEOCAPTURE);
    }

    public void setOutputFolder(String folder) {
        this.setConfigValue(this.OUTPUTFOLDER, folder);
    }

    public String getOutputFolder() {
        return this.getConfigValue(this.OUTPUTFOLDER);
    }

    public void setFPS(String fps) {
        this.setConfigValue(this.FPS, fps);
    }

    public int getFPS() {
        return Integer.parseInt(this.getConfigValue(this.FPS));
    }

}