package thesis.felix.Common.Config;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.logging.Logger;

/***
 * @brief Class to read and write property files
 */
public class ConfigBase {

    private static final Logger log = Logger.getLogger(ConfigBase.class.getName());

    private Properties config;
    private String file;

    public ConfigBase(String file) {
        this.file = file;
        this.config = new Properties();
        this.readConfigFile();
    }

    private void readConfigFile() {
        try {
            new FileOutputStream(this.file, true);
            log.info("Configuration file created sucessfully");
        } catch (IOException ex) {
            ex.printStackTrace();
            log.severe(String.format("Unable to create the configuration file %s, exiting", this.file));
            System.exit(0);
        }
        try (InputStream input = new FileInputStream(this.file)) {
            // load a properties file
            this.config.load(input);
            log.info("Configuration file loaded sucessfully");
        } catch (IOException ex) {
            ex.printStackTrace();
            log.severe(String.format("Unable to load configuration file %s, exiting", this.file));
            System.exit(0);
        }
    }

    public String getConfigValue(String key) {
        return this.config.getProperty(key);
    }

    public void setConfigValue(String key, String value) {
        this.config.setProperty(key, value);

        try (OutputStream output = new FileOutputStream(this.file)) {

            this.config.store(output, null);

        } catch (IOException io) {
            io.printStackTrace();
        }

        this.readConfigFile();
    }
}