package thesis.felix.Common;

import thesis.felix.interfaces.IStopWatch;

import java.util.concurrent.TimeUnit;

public class StopWatch implements IStopWatch {
    protected long startTime;
    protected long endTime;
    protected boolean running;

    public StopWatch() {
        this.running = false;
    }


    @Override
    public void reset() {
        this.running = false;
        this.startTime = 0;
        this.endTime = 0;
    }

    @Override
    public void startWatch() {
        if (!this.running) {
            this.startTime = System.currentTimeMillis();
            this.running = true;
        }/// todo error?
    }

    @Override
    public void stopWatch() {
        if (this.running) {
            this.endTime = System.currentTimeMillis();
            this.running = false;
        }
    }

    @Override
    public long getTimeInMilliseconds() {
        if (!this.running) {
            return this.endTime - this.startTime;
        }
        return 0;
    }

    @Override
    public float getTimeInSeconds() {
        if (!this.running) {
            return  Math.abs((float) (this.startTime - this.endTime) / 1000);
        }

        return 0;
    }
}
