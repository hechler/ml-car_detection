package thesis.felix.Common;

public class Camera {

    private int focalLengthMM;
    private double sensorHeightMM;
    private int averageHeightMM;
    private int imageHeightPixel;

    public Camera() {
        this.focalLengthMM = 48;
        this.averageHeightMM = 1400;
        this.sensorHeightMM = 15.66;
        this.imageHeightPixel = 1080;
    }

    /***
     * @brief Will return the calculated distance of the object to the camer
     * @param objectHeightPixels
     */
    public double getDistance(int objectHeightPixels) {
        return ((this.focalLengthMM * this.averageHeightMM * this.imageHeightPixel)
                / (objectHeightPixels * this.sensorHeightMM));
    }

}