package thesis.felix.Common;

import java.util.Scanner;

public class ScannerClean {
    protected Scanner scanner;
    private static ScannerClean instance;

    private ScannerClean() {
        this.scanner = new Scanner(System.in);
    }

    public static ScannerClean getInstance() {
        if (instance == null) {
            instance = new ScannerClean();
        }
        return instance;
    }

    public int nextInt() {
        int re = scanner.nextInt();
        scanner.nextLine();
        return re;
    }

    public String nextLine() {
        return scanner.nextLine();
    }


}
