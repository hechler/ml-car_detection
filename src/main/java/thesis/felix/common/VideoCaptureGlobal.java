package thesis.felix.Common;

import org.opencv.videoio.VideoCapture;

import thesis.felix.Common.Config.ApplicationConfig;

public class VideoCaptureGlobal {
    private static VideoCapture instance = null;

    public static VideoCapture getInstance() {

        if (instance == null) {
            instance = new VideoCapture();

            ApplicationConfig config = new ApplicationConfig();
            if (Helpers.isNumeric(config.getVideoCapture())) {
                instance.open(Integer.parseInt(config.getVideoCapture()));
            } else {
                instance.open(config.getVideoCapture());
            }

        }

        return instance;

    }
}