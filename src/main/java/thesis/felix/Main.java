package thesis.felix;

import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.dnn.Dnn;
import org.opencv.dnn.Net;

import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.VideoWriter;
import org.opencv.videoio.Videoio;

import thesis.felix.ApplicationConfig.TakeRoi;
import thesis.felix.Classifiers.ClassifierFactory;
import thesis.felix.Classifiers.ClassifierNames;
import thesis.felix.Classifiers.ClassifierObject;
import thesis.felix.Classifiers.interfaces.IClassifierFactory;
import thesis.felix.Classifiers.interfaces.IDnnClassifier;
import thesis.felix.Classifiers.yolo.Yolo3ClassNames;
import thesis.felix.Comparison.CompareNets;
import thesis.felix.Recorder.Recorder;
import thesis.felix.ThreadControl.EndProgramm;
import thesis.felix.Tracker.Interfaces.ITracker;
import thesis.felix.Tracker.ObjectTracker;
import thesis.felix.Worker.Worker;
import thesis.felix.Common.Config.ApplicationConfig;
import thesis.felix.Common.Config.ConfigBase;
import thesis.felix.Common.FrameObject;
import thesis.felix.Common.ScannerClean;
import thesis.felix.Common.StopWatch;
import thesis.felix.Common.VideoCaptureGlobal;
import thesis.felix.Classifiers.interfaces.IClassifierClassName;
import thesis.felix.interfaces.IStopWatch;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.util.*;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.regex.Pattern;

/***
 * @brief This is the start of the programm The programm can load and detect
 *        objects on images
 */
public class Main {

    public static List<String> getOutputNames(Net net) {
        List<String> names = new ArrayList<>();

        List<Integer> outLayers = net.getUnconnectedOutLayers().toList();
        List<String> layersNames = net.getLayerNames();

        outLayers.forEach((item) -> names.add(layersNames.get(item - 1)));// unfold and create R-CNN layers from the
                                                                          // loaded YOLO model//
        return names;
    }

    public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException,
            InstantiationException, IllegalAccessException, NoSuchFieldException, InterruptedException {

        nu.pattern.OpenCV.loadLocally();
        // CompareNets compr = new CompareNets();
        // compr.compare();

        ConcurrentLinkedDeque<FrameObject> queue = new ConcurrentLinkedDeque<>();

        ReadWriteLock rwLock = new ReentrantReadWriteLock();

        ApplicationConfig config = new ApplicationConfig();

        System.out.printf("Do you want to set the configuration file?\n 0: No \t 1:Yes\n");
        int decision = ScannerClean.getInstance().nextInt();

        if (decision == 1) {
            config.setApplicationConfig();
        }

        VideoCapture camera = VideoCaptureGlobal.getInstance();

        Worker worker = new Worker(true, queue, rwLock);

        Thread workerThread = new Thread(worker);
        workerThread.start();

        StopWatch sw = new StopWatch();
        sw.startWatch();

        Timer frameTimer = new Timer();
        frameTimer.scheduleAtFixedRate(new TimerTask() {

            Mat frame = new Mat();

            @Override
            public void run() {

                if (camera.read(frame)) {
                    FrameObject tmp = new FrameObject(frame);
                    queue.add(tmp);
                } else {
                    rwLock.writeLock().lock();

                    EndProgramm.endProgramm = true;

                    rwLock.writeLock().unlock();
                }

                this.checkIfTerminate();

            }

            private void checkIfTerminate() {
                rwLock.readLock().lock();

                if (EndProgramm.endProgramm) {
                    System.out.println("Cancel Queue output");
                    frameTimer.cancel();
                    frameTimer.purge();
                }

                rwLock.readLock().unlock();

            }

        }, 0, (1000 / config.getFPS()));

        Timer queueTimer = new Timer();
        queueTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                System.out.println(queue.size());
                this.checkIfTerminate();
            }

            private void checkIfTerminate() {
                rwLock.readLock().lock();

                if (EndProgramm.endProgramm) {
                    System.out.println("Cancel Queue output");
                    queueTimer.cancel();
                    queueTimer.purge();
                }

                rwLock.readLock().unlock();

            }
        }, 0, 1000);

        Recorder recorder = new Recorder(rwLock, queue);

        Thread recorderThread = new Thread(recorder);
        recorderThread.start();

        System.out.println("End with c: ");

        // String input = ScannerClean.getInstance().nextLine();

        // if (input.equals("c")) {
        // rwLock.writeLock().lock();

        // EndProgramm.endProgramm = true;

        // rwLock.writeLock().unlock();

        // }

        workerThread.join();
        recorderThread.join();
        sw.stopWatch();

        System.out.printf("Time: %f", sw.getTimeInSeconds());

        System.exit(0);
        // camera.read(framef);
        // VideoWriter vw = new VideoWriter("/Volumes/Data/outt.mp4",
        // VideoWriter.fourcc('X', 'V', 'I', 'D'), 10, new Size(framef.cols(),
        // framef.rows()));

    }

}
