package thesis.felix.ApplicationConfig;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.highgui.HighGui;
import org.opencv.imgproc.Imgproc;

public class TakeRoi {

    private final String frameName = "Roi";

    public TakeRoi() {
    }

    public Rect placeHolder(Mat frame) {

        HighGui.imshow(this.frameName, frame);
        HighGui.windows.get(this.frameName).flag = 1;
        HighGui.waitKey(1);

        final Point[] p1 = { new Point() };

        final Point[] p2 = { new Point() };

        HighGui.windows.get(this.frameName).frame.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {
                System.out.printf("X: %d\nY: %d\n", e.getX(), e.getY());
                p1[0] = new Point(e.getX(), e.getY());
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                System.out.printf("X: %d\nY: %d\n", e.getX(), e.getY());
                p2[0] = new Point(e.getX(), e.getY());
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        HighGui.imshow(this.frameName, frame);
        HighGui.waitKey();

        Imgproc.rectangle(frame, p1[0], p2[0], new Scalar(255, 0, 0));

        System.out.println("height : " + (p2[0].y - p1[0].y));

        HighGui.imshow(this.frameName, frame);
        HighGui.waitKey(1);

        HighGui.destroyAllWindows();

        return new Rect(p1[0], p2[0]);
    }

}