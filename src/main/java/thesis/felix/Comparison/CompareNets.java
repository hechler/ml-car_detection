package thesis.felix.Comparison;

import org.opencv.core.*;
import org.opencv.dnn.Dnn;
import org.opencv.dnn.Net;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import thesis.felix.Classifiers.ClassifierFactory;
import thesis.felix.Classifiers.ClassifierNames;
import thesis.felix.Classifiers.ClassifierObject;
import thesis.felix.Classifiers.interfaces.IClassifierClassName;
import thesis.felix.Classifiers.interfaces.IClassifierFactory;
import thesis.felix.Classifiers.interfaces.IDnnClassifier;
import thesis.felix.Main;
import thesis.felix.Common.ScannerClean;
import thesis.felix.Common.StopWatch;
import thesis.felix.interfaces.IStopWatch;

import java.io.File;
import java.lang.reflect.Field;
import java.util.*;

public class CompareNets {

    private Vector<Integer> indexOfNetsToCompare = new Vector<>();
    private Map<String, IDnnClassifier> nets = new HashMap<>();
    private ScannerClean scanner = ScannerClean.getInstance();
    private String inputPath = "";
    private int imageLimit = 0;

    public CompareNets(boolean non) {

        IClassifierFactory factory = ClassifierFactory.getInstance();

        try {
            Field[] fields = ClassifierNames.class.getFields();

            Field field = ClassifierNames.class.getField(fields[1].getName());

            this.nets.put(fields[1].getName(), factory.getNetwork((ClassifierObject) field.get(ClassifierNames.class)));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public CompareNets() {// todo refactor: split up

        Field[] fields = ClassifierNames.class.getFields();

        System.out.println(String.format("How many nets you want to compare? (1 - %d)", fields.length));

        int amountToCompare = this.scanner.nextInt();

        for (int i = 0; i < amountToCompare; i++) {
            System.out.println("Enter Number for NN");
            for (int j = 0; j < fields.length; j++) {

                if (!indexOfNetsToCompare.contains(j)) {
                    System.out.println(String.format("%d : %s", j, fields[j].getName()));

                }
            }
            this.indexOfNetsToCompare.add(this.scanner.nextInt());

        }

        setPathOfIamgeFolderByInput();
        setInputLimitByInput();

        IClassifierFactory factory = ClassifierFactory.getInstance();

        this.indexOfNetsToCompare.forEach((value) -> {
            try {

                Field field = ClassifierNames.class.getField(fields[value].getName());

                this.nets.put(fields[value].getName(),
                        factory.getNetwork((ClassifierObject) field.get(ClassifierNames.class)));

            } catch (Exception e) {
                e.printStackTrace();
            }

        });

    }

    void setPathOfImage(String path) {
        this.inputPath = path;
    }

    public void setPathOfIamgeFolderByInput() {
        System.out.println("Enter path");

        this.setPathOfImage(this.scanner.nextLine());
    }

    void setInputLimit(int limit) {
        this.imageLimit = Math.min(limit,
                Objects.requireNonNull(new File(this.inputPath).listFiles(File::isFile)).length);
    }

    public void setInputLimitByInput() {

        System.out.println(String.format("Enter limit(Hard Limit: %d)",
                Objects.requireNonNull(new File(this.inputPath).listFiles(File::isFile)).length));

        this.setInputLimit(this.scanner.nextInt()); // Read user input
    }

    public void compare() {

        File[] imageFiles = new File(this.inputPath).listFiles(File::isFile);

        List<CompareResult> compareResults = new ArrayList<>();

        this.nets.forEach((classifierName, classifier) -> {

            IStopWatch stopWatch = new StopWatch();

            stopWatch.startWatch();

            IClassifierClassName classNames = classifier.getClassNameObject();

            Net net = classifier.getNet();

            int foundAmount = 0;

            List<String> notFoundList = new ArrayList<>();

            outer: for (int index = 1; index <= imageLimit; index++) {

                System.out.println(String.format("Start Processing: %d of %d", index, imageLimit));

                Mat frame = Imgcodecs.imread(imageFiles[index - 1].getPath());

                Imgproc.resize(frame, frame, classifier.getImageSize());

                List<Mat> result = new ArrayList<>();
                List<String> outBlobNames = Main.getOutputNames(net);

                Mat blob = Dnn.blobFromImage(frame, 0.00392, classifier.getImageSize(), new Scalar(0), true, false); // We
                                                                                                                     // feed
                                                                                                                     // one
                                                                                                                     // frame
                                                                                                                     // of
                                                                                                                     // video
                                                                                                                     // into
                                                                                                                     // the
                                                                                                                     // network
                                                                                                                     // at
                                                                                                                     // a
                                                                                                                     // time,
                                                                                                                     // we
                                                                                                                     // have
                                                                                                                     // to
                                                                                                                     // convert
                                                                                                                     // the
                                                                                                                     // image
                                                                                                                     // to
                                                                                                                     // a
                                                                                                                     // blob.
                                                                                                                     // A
                                                                                                                     // blob
                                                                                                                     // is
                                                                                                                     // a
                                                                                                                     // pre-processed
                                                                                                                     // image
                                                                                                                     // that
                                                                                                                     // serves
                                                                                                                     // as
                                                                                                                     // the
                                                                                                                     // input.//

                // Mat blob = Dnn.blobFromImage(frame ); // We feed one frame of video into the
                // network at a time, we have to convert the image to a blob. A blob is a
                // pre-processed image that serves as the input.//

                net.setInput(blob);

                // Feed forward the model to get output //
                outBlobNames.forEach((name) -> {
                    // result.add(net.forward(name));
                });

                result.add(net.forward(outBlobNames.get(0)));

                System.out.println("Size: " + result.size());

                // outBlobNames.forEach(System.out::println);
                // result.forEach(System.out::println);

                float confThreshold = 0.5f; // Insert thresholding beyond which the model will detect objects//
                List<Integer> clsIds = new ArrayList<>();
                List<Float> confs = new ArrayList<>();
                List<Rect> rects = new ArrayList<>();

                for (Mat level : result) {
                    // each row is a candidate detection, the 1st 4 numbers are
                    // [center_x, center_y, width, height], followed by (N-4) class probabilities
                    for (int j = 0; j < level.rows(); ++j) {
                        Mat row = level.row(j);
                        Mat scores = row.colRange(5, level.cols());
                        Core.MinMaxLocResult mm = Core.minMaxLoc(scores);
                        float confidence = (float) mm.maxVal;
                        Point classIdPoint = mm.maxLoc;
                        if (confidence > confThreshold) {

                            int centerX = (int) (row.get(0, 0)[0] * frame.cols()); // scaling for drawing the bounding
                                                                                   // boxes//
                            int centerY = (int) (row.get(0, 1)[0] * frame.rows());

                            int width = (int) (row.get(0, 2)[0] * frame.cols());
                            int height = (int) (row.get(0, 3)[0] * frame.rows());

                            int left = centerX - width / 2;
                            int top = centerY - height / 2;

                            clsIds.add((int) classIdPoint.x);

                            if (classNames.getClassNameForIndex((int) classIdPoint.x).equals("car")
                                    || classNames.getClassNameForIndex((int) classIdPoint.x).equals("truck")) {
                                System.out.println(
                                        String.format("Found car in File: %s", imageFiles[index - 1].getPath()));
                                foundAmount++;

                                continue outer;
                            }

                        }
                    }
                }

                notFoundList.add(imageFiles[index - 1].getName());

            }

            // System.out.println("Done");

            // System.out.println("Not Found");
            // notFound.forEach(System.out::println);

            // System.out.println(String.format("Time Milliseconds: %d\nTime Seconds: %f
            // \nAverage Time in Milliseconds: %d", stopWatch.getTimeInMilliseconds(),
            // stopWatch.getTimeInSeconds(), (stopWatch.getTimeInMilliseconds() /
            // imageLimit)));

            stopWatch.stopWatch();

            compareResults.add(new CompareResult(classifierName, notFoundList, foundAmount, stopWatch));

            // System.out.println(String.format("Net: %s Found %d of %d
            // cars",classifierName, foundAmount, imageLimit));
        });

        System.out.print("Name\tFound\tNot Found\tRatio\tTime\tTime AVG\n");

        compareResults.forEach((object) -> {
            System.out.printf("%13s\t%d\t%d\t%f\t%f\t%d\n", object.getName(), object.getFoundAmount(),
                    object.getNotFoundAmount(), ((float) object.getFoundAmount() / imageLimit),
                    object.stopWatch.getTimeInSeconds(), (object.stopWatch.getTimeInMilliseconds() / imageLimit));
        });

    }
}
