package thesis.felix.Comparison;

import thesis.felix.Common.StopWatch;
import thesis.felix.interfaces.IStopWatch;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class CompareResult {
    private String myName;
    private List<String> notFound;
    private int foundAmount;
    public IStopWatch stopWatch;

    public CompareResult(String name, List<String> notFound, int foundAmount, IStopWatch stopWatch) {
        this.myName = name;

        this.notFound = new ArrayList<>();

        this.notFound = notFound;

        this.foundAmount = foundAmount;

        this.stopWatch = stopWatch;
    }

    public String getName() {
        return this.myName;
    }

    public int getFoundAmount() {
        return this.foundAmount;
    }

    public int getNotFoundAmount() {
        return this.notFound.size();
    }
}
