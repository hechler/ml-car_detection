# Implementation of an Image Classifier using Neural Networks and OpenCV

##OpenCV

It is important to note that the video writer of opencv uses FFMPEG on windows and linux and AVFoundation on mac. The programms and the codecs have to be installed.

##Download extra files

### ``wget -P extras/ https://pjreddie.com/media/files/yolov3-tiny.weights``

### ```wget -P extras/ https://raw.githubusercontent.com/pjreddie/darknet/master/cfg/yolov3-tiny.cfg```

### ```https://drive.google.com/drive/folders/1uU-jHKbPUKhJiEYqcLdP-8haZLwNtyM-?usp=sharing```

The own trainied tiny config and model where added to the git repo. 

If any other network is wanted, it needs to be downloaded and put in the correct path. 

The path can be also be altered at classifiers/yolo and then setting them in the respective class.




## Run the program

#### Compile and run the program 

##### ``./run.sh``

#### Compile make full jar and run the program

##### ``./run.sh -f``

#### Compile and run tests

##### ``./run.sh -t``

