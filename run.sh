#/bin/sh

if [ $# -eq 0 ]; then
  mvn clean compile exec:java
else
  for arg in "$@"; do
    if [ "$arg" == "--full" ] || [ "$arg" == "-f" ]; then
      mvn package -T 8C

      java -cp target/thesis-code-1.0-SNAPSHOT-jar-with-dependencies.jar thesis.felix.Main

    elif [ "$arg" == "--test" ] || [ "$arg" == "-t" ]; then
      mvn test
    fi
  done
fi
